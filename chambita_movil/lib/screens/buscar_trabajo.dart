import 'dart:convert';

import 'package:chambita_movil/models/trabajo.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class BuscarTrabajo extends StatefulWidget {
  @override
  _BuscarTrabajoState createState() => _BuscarTrabajoState();
}

class _BuscarTrabajoState extends State<BuscarTrabajo> {
  List<Trabajo> _trabajos = List<Trabajo>();
  List<Trabajo> _trabajosDisplay = List<Trabajo>();

  Future<List<Trabajo>> fetchTrabajos() async {
    var url = 'http://10.0.2.2:3000/api/trabajos';
    var response = await http.get(url);
    var trabajos = List<Trabajo>();

    if (response.statusCode == 200) {
      var notesJson = json.decode(response.body);
      for (var noteJson in notesJson) {
        trabajos.add(Trabajo.fromJson(noteJson));
      }
    }
    return trabajos;
  }

  @override
  void initState() {
    fetchTrabajos().then((value) {
      setState(() {
        _trabajos.addAll(value);
        _trabajosDisplay = _trabajos;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int index = 10;
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                Color(0xFF012F3D),
                Color(0xFF0A4F64),
              ])),
        ),
        centerTitle: true,
        title: Image.asset(
          "assets/logos/logo_chambita__.png",
          scale: 22,
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,

            colors: [
              Color(0xFF012F3D),
              Color(0xFF012F3D),
              Color(0xFF0A4F64),
              Color(0xFF0A4F64),
            ],
            //stops: [0.1, 0.4, 0.6, 0.9],
          ),
        ),
        child: ListView.builder(
          itemBuilder: (context, index) {
            return index == 0 ? _searchBar() : _listItem(index - 1);
          },
          itemCount: _trabajosDisplay.length + 1,
        ),
      ),
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: TextField(
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        decoration: InputDecoration(
            hintText: "Buscar",
            hintStyle: TextStyle(
              color: Color(0xFF0A4F64),
            ),
            icon: Icon(
              Icons.search,
              color: Colors.white,
              size: 28,
            )),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _trabajosDisplay = _trabajos.where((Trabajo) {
              var titulo = Trabajo.nombreTrabajo.toLowerCase();
              return titulo.contains(text);
            }).toList();
          });
        },
      ),
    );
  }

  _listItem(index) {
    return Card(
      margin: EdgeInsets.only(left: 20, right: 20, top: 10),
      color: Color(0xff29404E),
      child: Padding(
        padding:
            const EdgeInsets.only(top: 32, bottom: 32, left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              _trabajosDisplay[index].nombreTrabajo.toString(),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              _trabajosDisplay[index].descTrabajo.toString(),
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
