import 'package:chambita_movil/helpers/http.dart';
import 'package:chambita_movil/helpers/http_response.dart';
import 'package:chambita_movil/models/authentication_response.dart';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart' show required;

class autenticationApi {
  final Http _http;
  autenticationApi(this._http);

  Future<HttpResponse<AuthenticationResponse>> register({
    @required String nombre,
    @required String correo,
    @required String clave,
  }) {
    return _http.request<AuthenticationResponse>(
      '/api/empleadores',
      method: "POST",
      data: {
        "dniEmpleador": 70551254,
        "nombre": nombre,
        "apellido": "---",
        "clave": clave,
        "numTelefono": 000000000,
        "correo": correo,
        "ciudad": "---",
        "distrito": "---",
        "foto": "",
        "cuentaValidada": 0
      },
      parser: (data) {
        return AuthenticationResponse.fromJson(data);
      },
    );
  }

  Future<HttpResponse<AuthenticationResponse>> loginSQL({
    @required String email,
    @required String password,
  }) async {
    return _http.request<AuthenticationResponse>(
      '/api/empleadores/findOne?_where=(correo,eq,usuario3@gmail.com)',
      method: "GET",
      data: {
        "clave": password,
        "correo": email,
      },
      parser: (data) {
        return AuthenticationResponse.fromJson(data);
      },
    );
  }

  Future<HttpResponse<AuthenticationResponse>> registroTrabajo({
    @required String nombreTrabajo,
    @required String descTrabajo,
    @required double monto,
    @required String fechaInicio,
    @required String fechaFin,
    @required String fechaPlazo,
    @required String distrito,
    @required String direccion,
  }) {
    return _http.request<AuthenticationResponse>(
      '/api/trabajo',
      method: "POST",
      data: {
        "nombreTrabajo": nombreTrabajo,
        "descTrabajo": descTrabajo,
        "pagoTrabajo": monto,
        "estado": 0,
        "fechaInicio": fechaInicio,
        "fechaFinal": fechaFin,
        "plazoPostulacion": fechaPlazo,
        "distrito": distrito,
        "direccion": direccion,
        "dniEmpleador": 11111111,
      },
      parser: (data) {
        return AuthenticationResponse.fromJson(data);
      },
    );
  }
}
