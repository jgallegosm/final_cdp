import 'package:test/test.dart';

int suma(int a, int b) {
  return a + b;
}

void main() {
  test("test that 1+1 = 2", () {
    var expected = 3;
    var funcion = suma(1, 1);
    expect(funcion, expected);
  });
}
