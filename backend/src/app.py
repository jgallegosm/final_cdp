from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app= Flask(__name__)
app.config ['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@localhost/chambita_cdp'
app.config ['SQLALCHEMY_TRACK_MODIFICATIONS']=False

db = SQLAlchemy(app)
ma = Marshmallow(app)




class usuario(db.Model):
    id=db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String(100))
    correo = db.Column(db.String(70), unique=True)
    clave = db.Column(db.String(100))
    
    def __init__(self,nombre,correo,clave):
        self.nombre = nombre
        self.correo = correo
        self.clave = clave



class trabajo(db.Model):
    id=db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String(100))
    descripcion = db.Column(db.String(100))
    monto = db.column(db.Integer)
    fechaInicio = db.Column(db.String(100))
    fechaFin = db.Column(db.String(100))
    fechaPlazo = db.Column(db.String(100))
    distrito = db.Column(db.String(100))
    direccion = db.Column(db.String(100))
    
    
    def __init__(self,nombre,descripcion, monto,fechaInicio,fechaFin,fechaPlazo,distrito,direccion):
        
        self.nombre = nombre
        self.descripcion = descripcion
        self.monto = monto
        self.fechaInicio = fechaInicio
        self.fechaFin = fechaFin
        self.fechaPlazo = fechaPlazo
        self.distrito = distrito
        self.direccion = direccion
        
        
    
db.create_all()

class TaskSchema(ma.Schema):
    class Meta:
        fields = ('id','nombre','correo','clave')
task_schema = TaskSchema()
tasks_schema = TaskSchema(many=True)

class TrabajoSchema(ma.Schema):
    class Meta:
        fields = ('id','nombreTrabajo','monto','fechaInicio','fechaFin','fechaPlazo','distrito','direccion')
trabajo_schema = TrabajoSchema()
trabajos_schema = TrabajoSchema(many=True)



@app.route('/api/user', methods=['POST'])
def create_task():
    nombre = request.json ['nombre']
    correo = request.json['correo']
    clave = request.json['clave']
    
    new_task = usuario(nombre, correo, clave)
    db.session.add(new_task)
    db.session.commit()
  
    return task_schema.jsonify(new_task)


@app.route('/api/user', methods=['GET'])
def get_usuarios():
    all_tasks = usuario.query.all()
    result = tasks_schema.dump(all_tasks)
    return jsonify(result)

@app.route('/api/user/<id>', methods=['GET'])
def get_usuario(id):
    task=usuario.query.get(id)
    return task_schema.jsonify(task)

@app.route("/api/user/<id>", methods=['PUT'])
def update_usuario(id):
    task = usuario.query.get(id)
    name = request.json['nombre']
    correo = request.json['correo']
    clave = request.json['clave']
    
    task.nombre = name
    task.correo = correo
    task.clave = task.clave
    
    db.session.commit()
    return task_schema.jsonify(task)

@app.route("/api/user/<id>",methods=['DELETE'])
def delete_usuario(id):
    task = usuario.query.get(id)
    db.session.delete(task)
    db.session.commit()
    return task_schema.jsonify(task)





@app.route('/api/trabajo', methods=['POST'])
def create_trabajo():

    nombre = request.json['nombreTrabajo']
    descripcion = request.json['descTrabajo']
    monto = request.json['monto']
    fechaInicio = request.json['fechaInicio']
    fechaFin = request.json['fechaFin']
    fechaPlazo = request.json['fechaPlazo']
    distrito = request.json['distrito']
    direccion = request.json['direccion']

    
    new_trabajo = trabajo(nombre,descripcion, monto,fechaInicio,fechaFin,fechaPlazo,distrito,direccion)
    db.session.add(new_trabajo)
    db.session.commit()
  
    return trabajo_schema.jsonify(new_trabajo)


@app.route('/api/trabajo', methods=['GET'])
def get_trabajos():
    all_trabajos = trabajo.query.all()
    result = trabajos_schema.dump(all_trabajos)
    results = trabajos_schema.dump(all_trabajos)
    #return trabajos_schema.jsonify
    return jsonify(result)

@app.route('/api/trabajo/<id>', methods=['GET'])
def get_trabajo(id):
    trabajo=trabajo.query.get(id)
    return trabajo_schema.jsonify(trabajo)

@app.route("/api/trabajo/<id>", methods=['PUT'])
def update_trabajo(id):
    trabajo = trabajo.query.get(id)
    nombre = request.json['nombreTrabajo']
    descripcion = request.json['descTrabajo']
    monto = request.json['monto']
    fechaInicio = request.json['fechaInicio']
    fechaFin = request.json['fechaFin']
    fechaPlazo = request.json['fechaPlazo']
    distrito = request.json['distrito']
    direccion = request.json['direccion']
    
    trabajo.nombre = request.json['nombre']
    trabajo.descripcion = request.json['descTrabajo']
    trabajo.monto = request.json['monto']
    trabajo.fechaInicio = request.json['fechaInicio']
    trabajo.fechaFin = request.json['fechaFin']
    trabajo.fechaPlazo = request.json['fechaPlazo']
    trabajo.distrito = request.json['distrito']
    trabajo.direccion = request.json['direccion']
    db.session.commit()
    return trabajo_schema.jsonify(trabajo)

@app.route("/api/trabajo/<id>",methods=['DELETE'])
def delete_trabajo(id):
    trabajo = trabajo.query.get(id)
    db.session.delete(trabajo)
    db.session.commit()
    return trabajo_schema.jsonify(trabajo)


@app.route("/",methods=['GET'])
def index():
    return jsonify({'mensaje': 'Biendvenidos a la API DE CHAMBITA'})

    

    
if __name__ == "__main__":
    app.run(debug=True)    
    
    