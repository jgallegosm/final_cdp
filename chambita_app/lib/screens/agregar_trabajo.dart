import 'dart:convert';

import 'package:chambita_app/api/autentication_api.dart';
import 'package:chambita_app/screens/home_screen.dart';
import 'package:chambita_app/utilities/constants.dart';
import 'package:chambita_app/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';

class AgregarTrabajo extends StatefulWidget {
  AgregarTrabajo({Key key}) : super(key: key);

  @override
  _AgregarTrabajoState createState() => _AgregarTrabajoState();
}

class _AgregarTrabajoState extends State<AgregarTrabajo> {
  TextEditingController txtNombreTrabajo = TextEditingController();
  TextEditingController txtDescTrabajo = TextEditingController();
  TextEditingController txtMontoTrabajo = TextEditingController();
  TextEditingController txtInicio = TextEditingController();
  TextEditingController txtFinal = TextEditingController();
  TextEditingController txtPlazoPostulacion = TextEditingController();
  TextEditingController txtDistrito = TextEditingController();
  TextEditingController txtDireccion = TextEditingController();

  Future<void> registroTrabajo(
    String nombreTrabajo,
    String descTrabajo,
    double monto,
    String fechaInicio,
    String fechaFin,
    String fechaPlazo,
    String distrito,
    String direccion,
  ) async {
    ProgressDialog.show(context);
    final _autenticationApi = GetIt.instance<autenticationApi>();

    final response = await _autenticationApi.registroTrabajo(
      nombreTrabajo: nombreTrabajo,
      descTrabajo: descTrabajo,
      monto: monto,
      fechaInicio: fechaInicio,
      fechaFin: fechaFin,
      fechaPlazo: fechaPlazo,
      distrito: distrito,
      direccion: direccion,
    );
    ProgressDialog.dissmiss(context);
    if (response.data != null) {
      print('esta bien SQL::: ${response.data}');
    } else {
      print("registro con error STATUSCODE ${response.error.statusCode}");
      print("registro con error MESSAGE ${response.error.message}");
      print("registro con error DATA ${response.error.data}");
      String message = response.error.message;
      if (response.error.statusCode == -1) {
        message = "No hay acceso a internet";
      } else if (response.error.statusCode == 409) {
        message =
            "Usuario Duplicado ${jsonEncode(response.error.data['duplicatedFields'])}";
      }
      Dialogs.alert(
        context,
        title: "ERROR",
        description: response.error.message,
      );
    }
  }

  @override
  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nombre del trabajo',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: txtNombreTrabajo,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.title,
                color: Colors.white,
              ),
              hintText: 'Ingresa nombre del trabajo',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Descripción del trabajo',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: txtDescTrabajo,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.description_outlined,
                color: Colors.white,
              ),
              hintText: 'Ingresar una descripción del trabajo',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _pagoTrabajoTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Ingrese el monto a pagar por hora',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: txtMontoTrabajo,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.monetization_on_rounded,
                color: Colors.white,
              ),
              hintText: 'Ingresar el monto a pagar por hora',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  void selectDate(BuildContext context, TextEditingController txt) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2000),
      lastDate: DateTime(2030),
    );
    if (picked != null) {
      print(picked.toString());
      txt.text = picked.toString();
      //txtInicio.text = picked.toString();
    }
  }

  Widget _dateInicio() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Ingrese la fecha de inicio',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            enableInteractiveSelection: false,
            keyboardType: TextInputType.datetime,
            controller: txtInicio,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.calendar_today,
                color: Colors.white,
              ),
              hintText: 'Ingresar la fecha de inicio',
              hintStyle: kHintTextStyle,
            ),
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              selectDate(context, txtInicio);
            },
          ),
        ),
      ],
    );
  }

  Widget _dateFin() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Ingrese la fecha de Fin',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            enableInteractiveSelection: false,
            keyboardType: TextInputType.datetime,
            controller: txtFinal,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.calendar_today,
                color: Colors.white,
              ),
              hintText: 'Ingresar la fecha de FIN',
              hintStyle: kHintTextStyle,
            ),
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              selectDate(context, txtFinal);
            },
          ),
        ),
      ],
    );
  }

  Widget _datePlazoPostulacion() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Ingrese la fecha de inicio',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            enableInteractiveSelection: false,
            keyboardType: TextInputType.datetime,
            controller: txtPlazoPostulacion,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.calendar_today,
                color: Colors.white,
              ),
              hintText: 'Ingresar la fecha de inicio',
              hintStyle: kHintTextStyle,
            ),
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              selectDate(context, txtPlazoPostulacion);
            },
          ),
        ),
      ],
    );
  }

  Widget _constDistrito() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Ingrese el distrito del trabajo',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: txtDistrito,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.place,
                color: Colors.white,
              ),
              hintText: 'Ingresar el distrito donde se realizará el trabajo',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _constDireccion() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Ingrese la dirección del trabajo',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: txtDireccion,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.place_sharp,
                color: Colors.white,
              ),
              hintText: 'Ingresar la dirección donde se realizará el trabajo',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildRegistrarTrabajoBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () async {
          registroTrabajo(
            txtNombreTrabajo.text.toString(),
            txtDescTrabajo.text.toString(),
            double.parse(txtMontoTrabajo.text),
            txtInicio.text.toString(),
            txtFinal.text.toString(),
            txtPlazoPostulacion.text.toString(),
            txtDistrito.text.toString(),
            txtDireccion.text.toString(),
          );

          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => HomeScreen(),
                //builder: (context) => VerifyPhoneScreen(),
              ));
        },
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.white,
        child: Text(
          'Registrar trabajo',
          style: TextStyle(
            color: Color(0xFF527DAA),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                Color(0xFF012F3D),
                Color(0xFF0A4F64),
              ])),
        ),
        centerTitle: true,
        title: Image.asset(
          "assets/logos/logo_chambita__.png",
          scale: 22,
        ),
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFF012F3D),
                      Color(0xFF012F3D),
                      Color(0xFF0A4F64),
                      Color(0xFF0A4F64),
                    ],
                    //stops: [0.1, 0.4, 0.6, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 30.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Agrega un trabajo',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 15.0),
                      _buildEmailTF(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _buildPasswordTF(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _pagoTrabajoTF(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _dateInicio(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _dateFin(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _datePlazoPostulacion(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _constDistrito(),
                      SizedBox(
                        height: 15.0,
                      ),
                      _constDireccion(),
                      _buildRegistrarTrabajoBtn(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
