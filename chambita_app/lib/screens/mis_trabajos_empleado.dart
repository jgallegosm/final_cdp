import 'package:chambita_app/models/trabajo.dart';
import 'package:chambita_app/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:chambita_app/data/data.dart';

import 'package:chambita_app/models/date_model.dart';

import 'package:chambita_app/models/event_type_model.dart';
import 'package:chambita_app/models/events_model.dart';
import 'package:path/path.dart';

class MisTrabajosEmpleado extends StatefulWidget {
  @override
  _MisTrabajosEmpleadoState createState() => _MisTrabajosEmpleadoState();
}

class _MisTrabajosEmpleadoState extends State<MisTrabajosEmpleado> {
  List<DateModel> dates = new List<DateModel>();
  List<EventTypeModel> eventsType = new List();
  List<EventsModel> events = new List<EventsModel>();
  List<Trabajo> _trabajos = List<Trabajo>();
  var url;
  @override
  void initState() {
    url = 'http://10.0.2.2:3000/api/trabajos?_where=(estado,eq,1)';
    fetchTrabajos(url).then((value) {
      setState(() {
        _trabajos.addAll(value);
      });
    });
    // TODO: implement initState
    super.initState();
    //dates = getDates();
    eventsType = getEventTypes();
    events = getEvents();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                Color(0xFF012F3D),
                Color(0xFF0A4F64),
              ])),
        ),
        centerTitle: true,
        title: Image.asset(
          "assets/logos/logo_chambita__.png",
          scale: 22,
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFF012F3D),
                    Color(0xFF012F3D),
                    Color(0xFF0A4F64),
                    Color(0xFF0A4F64),
                  ],
                ),
              ),
            ),
            //decoration: BoxDecoration(color: Color(0xff102733)),

            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 25, horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Hola, Frans!",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 21),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text(
                              "Bienvenido a la lista de tus trabajos...",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 15),
                            )
                          ],
                        ),
                        Spacer(),
                        Container(
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 4, color: Color(0xffFAE072)),
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: CircleAvatar(
                            radius: 35,
                            backgroundImage: AssetImage('assets/frans.jpg'),
                          ),
                        ),
                      ],
                    ),

                    /// Events
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Categorias",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 100,
                      child: ListView.builder(
                        itemCount: eventsType.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return EventTile(
                            imgAssetPath: eventsType[index].imgAssetPath,
                            eventType: eventsType[index].eventType,
                            url: eventsType[index].url,
                          );
                        },
                      ),
                    ),

                    /// Popular Events
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Listado",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: ListView.builder(
                          itemCount: _trabajos.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return PopularEventTile(
                              desc: _trabajos[index].nombreTrabajo,
                              imgeAssetPath: "assets/tileimg.png",
                              //imgeAssetPath: events[index].imgeAssetPath,
                              date: _trabajos[index].fechaFinal.toString(),
                              address: _trabajos[index].direccion,
                            );
                          }),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class EventTile extends StatefulWidget {
  String imgAssetPath;
  String eventType;
  var url;

  EventTile({this.imgAssetPath, this.eventType, this.url});

  @override
  _EventTileState createState() => _EventTileState();
}

class _EventTileState extends State<EventTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {print(widget.url)},
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 22),
        margin: EdgeInsets.only(right: 16),
        decoration: BoxDecoration(
          color: Color(0xff29404E),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              widget.imgAssetPath,
              height: 27,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              widget.eventType,
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}

class PopularEventTile extends StatefulWidget {
  String desc;
  String date;
  String address;
  String imgeAssetPath;

  /// later can be changed with imgUrl
  PopularEventTile({this.address, this.date, this.imgeAssetPath, this.desc});

  @override
  _PopularEventTileState createState() => _PopularEventTileState();
}

class _PopularEventTileState extends State<PopularEventTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          color: Color(0xff29404E), borderRadius: BorderRadius.circular(8)),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 16),
              width: MediaQuery.of(context).size.width - 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.desc,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/calender.png",
                        height: 20,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        widget.date,
                        style: TextStyle(color: Colors.white, fontSize: 13),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/location.png",
                        height: 20,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Flexible(
                        child: Text(
                          widget.address,
                          style: TextStyle(color: Colors.white, fontSize: 13),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          ClipRRect(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8)),
              child: Image.asset(
                widget.imgeAssetPath,
                height: 100,
                width: 120,
                fit: BoxFit.cover,
              )),
        ],
      ),
    );
  }
}
