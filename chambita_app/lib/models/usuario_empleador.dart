import 'package:meta/meta.dart' show required;

class UserEmpleador {
  UserEmpleador({
    @required this.dniEmpleador,
    @required this.nombre,
    @required this.apellido,
    @required this.clave,
    @required this.numTelefono,
    @required this.correo,
    @required this.ciudad,
    @required this.distrito,
    @required this.foto,
    @required this.cuentaValidada,
  });

  final int dniEmpleador;
  final String nombre;
  final String apellido;
  final String clave;
  final int numTelefono;
  final String correo;
  final String ciudad;
  final String distrito;
  final String foto;
  final int cuentaValidada;

  factory UserEmpleador.fromJson(Map<String, dynamic> json) => UserEmpleador(
        dniEmpleador: json["dniEmpleador"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        clave: json["clave"],
        numTelefono: json["numTelefono"],
        correo: json["correo"],
        ciudad: json["ciudad"],
        distrito: json["distrito"],
        foto: json["foto"],
        cuentaValidada: json["cuentaValidada"],
      );

  Map<String, dynamic> toJson() => {
        "dniEmpleador": dniEmpleador,
        "nombre": nombre,
        "apellido": apellido,
        "clave": clave,
        "numTelefono": numTelefono,
        "correo": correo,
        "ciudad": ciudad,
        "distrito": distrito,
        "foto": foto,
        "cuentaValidada": cuentaValidada,
      };
}
