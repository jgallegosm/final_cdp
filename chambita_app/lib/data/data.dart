import 'dart:convert';
import 'dart:math';

import 'package:chambita_app/models/date_model.dart';
import 'package:chambita_app/models/event_type_model.dart';
import 'package:chambita_app/models/events_model.dart';
import 'package:chambita_app/models/trabajo.dart';
import 'package:http/http.dart' as http;

/*
List<DateModel> getDates() {
  List<DateModel> dates = new List<DateModel>();
  DateModel dateModel = new DateModel();

  //1
  dateModel.date = "10";
  dateModel.weekDay = "Sun";
  dates.add(dateModel);

  dateModel = new DateModel();

  //1
  dateModel.date = "11";
  dateModel.weekDay = "Mon";
  dates.add(dateModel);

  dateModel = new DateModel();

  //1
  dateModel.date = "12";
  dateModel.weekDay = "Tue";
  dates.add(dateModel);

  dateModel = new DateModel();

  //1
  dateModel.date = "13";
  dateModel.weekDay = "Wed";
  dates.add(dateModel);

  dateModel = new DateModel();

  //1
  dateModel.date = "14";
  dateModel.weekDay = "Thu";
  dates.add(dateModel);

  dateModel = new DateModel();

  //1
  dateModel.date = "15";
  dateModel.weekDay = "Fri";
  dates.add(dateModel);

  dateModel = new DateModel();

  //1
  dateModel.date = "16";
  dateModel.weekDay = "Sat";
  dates.add(dateModel);

  dateModel = new DateModel();

  return dates;
}
*/
List<EventTypeModel> getEventTypes() {
  List<EventTypeModel> events = new List();
  EventTypeModel eventModel = new EventTypeModel();

  //1
  eventModel.imgAssetPath = "assets/Todos.png";
  eventModel.eventType = "Todos";
  eventModel.url = 'http://10.0.2.2:3000/api/trabajos';
  events.add(eventModel);

  eventModel = new EventTypeModel();

  //1
  eventModel.imgAssetPath = "assets/Activos.png";
  eventModel.eventType = "Activos";
  eventModel.url = 'http://10.0.2.2:3000/api/trabajos?_where=(estado,eq,1)';
  events.add(eventModel);

  eventModel = new EventTypeModel();

  //1
  eventModel.imgAssetPath = "assets/Inactivos.png";
  eventModel.eventType = "Inactivos";
  eventModel.url = 'http://10.0.2.2:3000/api/trabajos?_where=(estado,eq,0)';
  events.add(eventModel);

  //eventModel = new EventTypeModel();

  return events;
}

List<Trabajo> _trabajos = List<Trabajo>();
List<Trabajo> _trabajosDisplay = List<Trabajo>();

Future<List<Trabajo>> fetchTrabajos(var url) async {
  //url = 'http://10.0.2.2:3000/api/trabajos';
  var response = await http.get(url);
  var trabajos = List<Trabajo>();

  if (response.statusCode == 200) {
    print("conexion echa");
    var notesJson = json.decode(response.body);
    for (var noteJson in notesJson) {
      trabajos.add(Trabajo.fromJson(noteJson));
    }
  } else {
    print("no hace la conexion");
  }
  return trabajos;
}

List<EventsModel> getEvents() {
  List<EventsModel> events = new List<EventsModel>();
  EventsModel eventsModel = new EventsModel();
  List<Trabajo> _trabajos = List<Trabajo>();

  /*
  //1
  eventsModel.imgeAssetPath = "assets/tileimg.png";
  eventsModel.date = "Jan 12, 2019";
  eventsModel.desc = "ASISTENTE DE LO QUE SEA";
  eventsModel.address = "Calle mariano melgar, Cayma";
  events.add(eventsModel);

  eventsModel = new EventsModel();

  //2
  eventsModel.imgeAssetPath = "assets/second.png";
  eventsModel.date = "Jan 12, 2019";
  eventsModel.desc = "Computadoras, formateo";
  eventsModel.address = "Calle Juan Muñoz, Mariano Melgar";
  events.add(eventsModel);

  eventsModel = new EventsModel();

  //3
  eventsModel.imgeAssetPath = "assets/second.png";
  events.add(eventsModel);

  eventsModel = new EventsModel();
  int n = _trabajos.length;
  print("transkjp: $n");*/
/*
  List<String> dates = [
    "uno",
    "dos",
    "tres",
    "cuatro",
    "cinco",
    "seis,",
    "siete",
    "ocho"
  ];

  for (int i = 0; i < 8; i++) {
    eventsModel = new EventsModel();
    eventsModel.imgeAssetPath = "assets/music_event.png";
    eventsModel.date = dates[i];
    eventsModel.address = dates[i];
    eventsModel.desc = dates[i];
    events.add(eventsModel);
  }
  eventsModel = new EventsModel();*/
  //return events;
}
